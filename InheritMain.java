package test1;
import java.util.Scanner;

class Person {
	protected String name;
	protected String address;
	
	//Default Constructor
	public Person() {
		System.out.println("Inside Person : Constructor");
		name="";
		address="";
	}
	
	//Constructor dengan parameter
	public Person(String name,String address) {
//		Scanner inp =  new Scanner(System.in);
//		name = inp.nextLine();
//		address = inp.nextLine();
		this.name = name;
		this.address = address;
	}
	
	public String getName() {
		return name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public void job(String job)
	{
		job = "Mahasiswa";
		System.out.println("Pekerjaan "+job);
	}
	
	public void hobi(String hobi)
	{
		Scanner inp =  new Scanner(System.in);
		System.out.print("Hobi : ");
		hobi = inp.nextLine();
		System.out.println("Hobi "+hobi);
	}
	
	public void identity()
	{
		System.out.println("Nama: "+name);
		System.out.println("Alamat: "+address);
	}
}

class Student extends Person{
	String nim;
	int SPP = 2250000;
	int SKS = 250000;
	int modul = 30000;
	int tagihan = 0;
	int jmlhSKS = 0;
	int totalSKS = 0;
	int jmlhModul = 0; 
	int totalModul = 0;
	public Student() {
		System.out.println("Inside Student : Constructor");
		Scanner inp =  new Scanner(System.in);
		System.out.print("Nama : ");
		name = inp.nextLine();
		System.out.print("Alamat : ");
		address = inp.nextLine();
		super.name= name;
		super.address= address;
	}
	
	public String getNim()
	{
		Scanner inp =  new Scanner(System.in);
		System.out.print("NIM : ");
		String nim = inp.nextLine();
		return nim;
	}
	
	public int hitungPembayaran() {
		
		Scanner inp =  new Scanner(System.in);
		System.out.print("jumlah SKS: ");
		jmlhSKS = inp.nextInt();
		System.out.print("jumlah Modul: ");
		jmlhModul = inp.nextInt();
		
		totalSKS = jmlhSKS * SKS;
		totalModul = jmlhModul * modul;
		tagihan = SPP + totalSKS + totalModul;
		
		return tagihan;
		
	}
	
	@Override
	public void identity()
	{
		
		System.out.println("NIM: "+getNim());
		super.identity();
		System.out.println("Nama: "+super.name);
		System.out.println("Alamat: "+super.address);
		
	}
	
	public void job()
	{
		System.out.println("Pekerjaan : Mahasiswa");
	}
	
	public String getName() {
		System.out.println("Student Name: "+name);
		return name;
	}
		
}

public class InheritMain {
	
	public static void main(String[] args) {
		System.out.println("Class Student");
		Student mahasiswa  = new Student();
		mahasiswa.hitungPembayaran();
		mahasiswa.identity();
		mahasiswa.job();
		System.out.println("Jumlah SKS"+mahasiswa.jmlhSKS+" : "+mahasiswa.totalSKS);
		System.out.println("Jumlah Modul"+mahasiswa.jmlhModul+" : "+mahasiswa.totalModul);
		System.out.println("Tagihan SPP : "+mahasiswa.SPP);
		System.out.println("Jumlah total tagihan : "+mahasiswa.tagihan);
		
		System.out.println("\nClass Person");
		Person ref;
	
		Student studentObject = new Student();
//		Employee employeeObject = new Employee();
		
		ref = studentObject;
		ref.hobi("");
		String temp = ref.getName();
		String temp1 = ref.getAddress();
		System.out.println("Nama : "+temp);
		System.out.println("Alamat : "+temp1);
		ref.job("");
		
		
//		ref = employeeObject;
		
//		String temp1 = ref.getName();
//		System.out.println(temp);
	}

}